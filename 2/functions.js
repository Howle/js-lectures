function func1(num1, num2) {
  return num1 + num2;
}
const func2 = function (num1, num2) {
  return num1 + num2;
};
const func3 = (num1, num2) => {
  return num1 + num2;
};
const func4 = (num1, num2) => num1 + num2;

const functions = [func1, func2, func3, func4];

// functions.forEach((func, index, array) => {
//   console.log(`function ${index}: `, func(1, 2), func, array);
// });

function counterFunc(initialValue) {
  let initial = initialValue;
  return (num) => {
    initial += Number(num);
    console.log('Number is: ', initial);
    return initial;
  };
}

const counter = counterFunc(100);
counter(2);
counter(-99);
counter('2');
counter(null);
counter(1);
counter(1);
