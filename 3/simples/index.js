const http = require('http');
const fs = require('fs');

http
  .createServer((req, res) => {
    // console.log(req);
    const { url, method } = req;

    console.log('Url is: ', url, ' method is: ', method);
    switch (method) {
      case 'GET':
        if (url === '/') {
          const file = fs.readFileSync(__dirname + '/index.html', 'utf8');
          res.setHeader('Content-Type', 'text/html; charset=UTF-8');
          res.write(file);
          // console.log('File is: ', file);
        } else if (url === '/info') {
          res.write('Some information');
        } else {
          let file = '';
          try {
            file = fs.readFileSync(__dirname + url, 'utf8');
          } catch (error) {
            console.error(error);
          }
          // res.setHeader('Content-Type', 'text/html; charset=UTF-8');
          res.write(file);
        }
        // res.write('Holla todo el mundo!');

        break;
      case 'POST':
        break;

      default:
        break;
    }

    res.end();
  })
  .listen('9090');

// http.on('listen', () => {
//   console.log('Server starts on: http://localhost:9090 ');
// });
