const http = require('http');
const fs = require('fs');

const port = 5555;

const server = http.createServer(async (req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/html' });
  const { method, url } = req;
  console.log(method, url);
  switch (method) {
    case 'GET':
      if (url === '/') {
        try {
          const data = fs.readFileSync(`${__dirname}/index.html`, 'utf8');
          res.write(data);
        } catch (error) {
          console.log(error);
          return;
        }
      } else if (url === '/info') {
        const objToJson = {
          data: 'Server was created like example of native nodejs server',
        };
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify(objToJson));
        res.end();
      } else {
        const data = fs.readFileSync(`${__dirname}${url}`, 'utf8');
        res.write(data);
        res.end();
      }
      break;
    case 'POST':
      break;
    case 'PUT':
      break;

    default:
      res.write(`Unknown method: ${method}`);

      break;
  }
  res.end();
});

server.on('listening', () => {
  console.log(`Server starting on: http://localhost:${port}`);
});

server.listen(port);
