const TelegramBot = require('node-telegram-bot-api');
const dotenv = require('dotenv').config();
const bodyParser = require('body-parser');
const express = require('express');
const crypto = require('crypto');
const cookieParser = require('cookie-parser');
const { v4: uuid } = require('uuid');
const path = require('path');

const app = express();

// secret or salt to be hashed with
const secret = 'This is a company secret 🤫';

// create a sha-256 hasher
const sha256Hasher = crypto.createHmac('sha256', secret);

const token = process.env.TG_TOKEN;

const users = {
  'nikkoz@yandex.ru': {
    name: 'Nikolay',
    lastname: 'Kozimir',
    email: 'nikkoz@yandex.ru',
    telegram: '@Howle',
    telegramCode: '3241200',
    password: sha256Hasher.update('NKPass'),
  },
  't@t.t': {
    name: 'Test',
    lastname: 'Testovich',
    email: 't@t.t',
    telegram: '@Howle',
    chatId: 303272045,
    telegramCode: '324123',
    password: sha256Hasher.update('test'),
  },
};

const sessions = {};
const temporarySessions = {};

const bot = new TelegramBot(token, { polling: true });

bot.onText(/\/auth (.+)/, (msg, match) => {
  const chatId = msg.chat.id;
  const nickname = msg.chat.username;
  const code = match[1];
  try {
    const user = Object.entries(users).find(
      ([key, value]) => value.telegramCode === code,
    );
    console.log(user);
    if (user) {
      users[user[0]].chatId = chatId;
      users[user[0]].telegram = nickname;
      bot.sendMessage(
        chatId,
        'User successfully linked. Now you can freely login with telegram auth if it will necessary.',
      );
    } else {
      bot.sendMessage(
        chatId,
        "Can't find user with this code. Please try enter code again.",
      );
    }
  } catch (error) {
    bot.sendMessage(chatId, 'Something go wrong. Please try it later');
  }
});

// bot.onText(/(.+)/, (msg) => {
//   const chatId = msg.chat.id;
//   bot.sendMessage(
//     chatId,
//     'To authenticate in application go to your profile page, find bot code and send it to me in format: /auth <code>',
//   );
// });

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cookieParser());

app.get('/', (req, res) => {
  res.send('Hello!');
});

app.get('/users', (req, res) => {
  console.log('!!! users');
  res.send({ users });
});

app.post('/singup', (req, res) => {
  const { body } = req;
  const { name, lastname, email, password, telegram } = body;

  if (!(name && lastname && email && password)) {
    res.status(400).json({ error: "Haven't enough data to sign up user" });
  }

  const oldUser = users[email];

  if (oldUser) {
    res.status(400).json({ error: "You're have signed up yet" });
  }

  users[email] = {
    name,
    lastname,
    password: sha256Hasher.update(password),
    telegramCode: Math.random().toString().substr(2, 6),
    email,
  };

  const id = uuid();
  sessions[id] = email;

  console.log({ users });

  res.cookie('session', id, { expires: new Date(Date.now() + 1000 * 60 * 10) });
  res.status(201).json({ id });
});

app.post('/signin', (req, res) => {
  const { email, password } = req.body;

  if (!password || !email) {
    return res.status(400).json({ error: 'Не указан E-Mail или пароль' });
  }

  const hashedPassword = sha256Hasher.update(password);
  if (!users[email] || users[email].password !== hashedPassword) {
    return res.status(400).json({ error: 'Не верный E-Mail и/или пароль' });
  }

  const id = uuid();
  sessions[id] = email;

  res.cookie('session', id, {
    expires: new Date(Date.now() + 1000 * 60 * 10),
  });
  res.status(201).json({ id });
});

app.post('/signout', (req, res) => {
  let id, temporaryId;
  try {
    id = req.cookies['session'];
    temporaryId = req.cookies['temporarySessions'];
  } catch (error) {
    if (!id) {
      res.status(401).json({
        error: 'Please sign in in the system to have access in this section',
      });
    }
  }

  delete sessions?.[id];
  delete temporarySessions?.[temporaryId];

  res.json(id);
});

app.get('/me', function (req, res) {
  let id;
  try {
    console.log('ME Cookie: ', req.cookies['session']);
    id = req.cookies['session'];
    console.log('ME id: ', req.cookies['session']);
  } catch (error) {
    if (!id) {
      res.status(401).json({
        error: 'Please sign in in the system to have access in this section',
      });
    }
  }

  try {
    const email = sessions[id];
    if (!email || !users[email]) {
      return res.status(401).end();
    }

    const user = users[email];

    res.status(200).json(user);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error });
  }
});

app.post('/auth/telegram/email', (req, res) => {
  const { email } = req.body;
  const user = users[email];
  if (user.telegram) {
    user.code = Math.random().toString().substr(2, 4);
    bot.sendMessage(user.chatId, `You're secrite code is: ${user.code}`);

    const id = uuid();
    temporarySessions[id] = email;
    res.cookie('temporarySessions', id, {
      expires: new Date(Date.now() + 1000 * 60 * 5),
    });
    res.status(201).json({ id });
  } else {
    res.status(401).json({ error: "You haven't linked telegram account" });
  }
});

app.post('/auth/telegram/code', (req, res) => {
  const { code } = req.body;
  let id;
  id = req.cookies['temporarySessions'];
  if (!id) {
    res.status(401).json({
      error: 'Please get new telegram auth code.',
    });
  } else {
    const email = temporarySessions[id];
    const user = users[email];
    if (!!user.code && user.code == code) {
      const newId = uuid();
      sessions[newId] = email;

      res.cookie('session', newId, {
        expires: new Date(Date.now() + 1000 * 60 * 10),
      });
      console.log('Code newId: ', { id: newId });
      res.status(201).json({ id: newId });
    } else {
      res.status(400).json({ error: 'Incorrect code' });
    }
  }
});

app.listen('9090', () => {
  console.log('Server is working on http://localhost:9090!');
});
