## To start:

1. Run command `npm i`.
2. Copy `.env.example` and rename it to `.env`. Then replace template on your telegram token.
3. Start server with: `npm run dev`.
4. Go to frontend app by [link](http://localhost:9090/index.html).
5. Enjoy your life =P