import {
  MainPage,
  SignInPage,
  TelegramEmailPage,
  TelegramCodePage,
} from './pages.js';

export const application = document.getElementById('app');

export const routes = {
  welcomePage: {
    render: () => SignInPage(application).render(),
    auth: false,
  },
  telegramEmail: {
    render: () => TelegramEmailPage(application).render(),
    auth: false,
  },
  telegramCode: {
    render: () => TelegramCodePage(application).render(),
    auth: false,
  },
  mainPage: { render: () => MainPage(application).render(), auth: true },
};

export const redirect = (section) => {
  application.innerHTML = '';

  if (routes[section].auth && !window.store.auth) {
    routes['welcomePage'].render();
  } else {
    routes[section].render();
  }
};
