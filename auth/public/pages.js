import {
  signUpRequest,
  signInRequest,
  signOutRequest,
  meRequest,
  getTelegramCodeRequest,
  checkTelegramCodeRequest,
} from './network.js';

import { redirect } from './utils.js';

export const SignUpComponent = (root) => {
  let wrapper;

  const render = () => {
    wrapper = document.createElement('form');
    wrapper.className = 'signup';

    const header = document.createElement('h3');
    header.className = 'signup-header';
    header.innerText = 'Sign up';
    const containerBlock = document.createElement('div');
    containerBlock.className = 'container';

    const nameContainer = document.createElement('div');
    nameContainer.className = 'input-container name';
    const lastnameContainer = document.createElement('div');
    lastnameContainer.className = 'input-container lastname';
    const emailContainer = document.createElement('div');
    emailContainer.className = 'input-container email';
    const passwordContainer = document.createElement('div');
    passwordContainer.className = 'input-container password';
    const buttonContainer = document.createElement('div');
    buttonContainer.className = 'button-wrapper';

    const nameLabel = document.createElement('label');
    nameLabel.for = 'name';
    nameLabel.innerText = 'Name';
    const nameInput = document.createElement('input');
    nameInput.id = 'name';
    nameInput.type = 'text';
    nameInput.tabindex = 0;
    const lastnameLabel = document.createElement('label');
    lastnameLabel.for = 'lastname';
    lastnameLabel.innerText = 'Lastname';
    const lastnameInput = document.createElement('input');
    lastnameInput.id = 'lastname';
    lastnameInput.type = 'text';
    lastnameInput.tabindex = 0;
    const emailLabel = document.createElement('label');
    emailLabel.for = 'email';
    emailLabel.innerText = 'Email';
    const emailInput = document.createElement('input');
    emailInput.id = 'email';
    emailInput.type = 'email';
    emailInput.tabindex = 0;
    const passwordLabel = document.createElement('label');
    passwordLabel.for = 'password';
    passwordLabel.innerText = 'Password';
    const passwordInput = document.createElement('input');
    passwordInput.id = 'password';
    passwordInput.type = 'password';
    passwordInput.tabindex = 0;
    const signUpButton = document.createElement('button');
    signUpButton.innerText = 'Sign up';
    signUpButton.tabindex = 0;

    nameContainer.appendChild(nameLabel);
    nameContainer.appendChild(nameInput);
    lastnameContainer.appendChild(lastnameLabel);
    lastnameContainer.appendChild(lastnameInput);
    emailContainer.appendChild(emailLabel);
    emailContainer.appendChild(emailInput);
    passwordContainer.appendChild(passwordLabel);
    passwordContainer.appendChild(passwordInput);
    buttonContainer.appendChild(signUpButton);

    containerBlock.appendChild(nameContainer);
    containerBlock.appendChild(lastnameContainer);
    containerBlock.appendChild(emailContainer);
    containerBlock.appendChild(passwordContainer);
    containerBlock.appendChild(buttonContainer);

    wrapper.appendChild(header);
    wrapper.appendChild(containerBlock);

    root.appendChild(wrapper);

    signUpButton.addEventListener('click', (event) => {
      event.preventDefault();

      const inputs = wrapper.getElementsByTagName('input');
      let newUser = {};
      for (const elem of inputs) {
        newUser[elem.id] = elem.value;
      }
      console.log({ newUser });

      signUpRequest(newUser).then(() => redirect('mainPage'));

      // hide();
    });
  };

  const hide = () => {
    root.removeChild(wrapper);
  };

  return {
    render,
    hide,
  };
};

export const SignInComponent = (root) => {
  let wrapper = document.createElement('form');
  wrapper.className = 'signin';

  const header = document.createElement('h3');
  header.className = 'signin-header';
  header.innerText = 'Welcome';
  const containerBlock = document.createElement('div');
  containerBlock.className = 'container';

  const emailContainer = document.createElement('div');
  emailContainer.className = 'input-container email';
  const passwordContainer = document.createElement('div');
  passwordContainer.className = 'input-container password';
  const buttonContainer = document.createElement('div');
  buttonContainer.className = 'button-wrapper';

  const emailLabel = document.createElement('label');
  emailLabel.for = 'email';
  emailLabel.innerText = 'Email';
  const emailInput = document.createElement('input');
  emailInput.id = 'email';
  emailInput.type = 'email';
  emailInput.tabindex = 0;
  const passwordLabel = document.createElement('label');
  passwordLabel.for = 'password';
  passwordLabel.innerText = 'Password';
  const passwordInput = document.createElement('input');
  passwordInput.id = 'password';
  passwordInput.type = 'password';
  passwordInput.tabindex = 0;

  const signInButton = document.createElement('button');
  signInButton.innerText = 'Sign in';
  const render = () => {
    emailContainer.appendChild(emailLabel);
    emailContainer.appendChild(emailInput);
    passwordContainer.appendChild(passwordLabel);
    passwordContainer.appendChild(passwordInput);
    buttonContainer.appendChild(signInButton);

    containerBlock.appendChild(emailContainer);
    containerBlock.appendChild(passwordContainer);
    containerBlock.appendChild(buttonContainer);

    wrapper.appendChild(header);
    wrapper.appendChild(containerBlock);

    root.appendChild(wrapper);

    signInButton.addEventListener('click', (event) => {
      event.preventDefault();

      const inputs = wrapper.getElementsByTagName('input');

      let user = {};
      for (const elem of inputs) {
        user[elem.id] = elem.value;
      }

      signInRequest(user).then(() => redirect('mainPage'));

      hide();
    });
  };

  const hide = () => {
    root.removeChild(wrapper);
  };

  return {
    render,
  };
};

export const MainPage = (root) => {
  let wrapper;

  const render = () => {
    root.innerHTML = '';

    wrapper = document.createElement('div');
    wrapper.className = 'wrapper_user_data';

    const userBlock = document.createElement('div');
    userBlock.className = 'user';
    const signoutBlock = document.createElement('div');
    signoutBlock.className = 'signout';

    const nameBlock = document.createElement('div');
    nameBlock.className = 'name';
    const lastnameBlock = document.createElement('div');
    lastnameBlock.className = 'lastname';
    const emailBlock = document.createElement('div');
    emailBlock.className = 'email';
    const telegramBlock = document.createElement('div');
    telegramBlock.className = 'telegram';

    const telegramDescription = document.createElement('div');
    telegramDescription.className = 'telegram_text';
    const telegramLink = document.createElement('a');
    telegramLink.className = 'telegram_link';
    telegramLink.innerText =
      'Перейти к телеграм боту для упрощённой авторизации';
    telegramLink.target = '_blank';
    telegramLink.href = 'https://t.me/howles1stbot';

    const signOutButton = document.createElement('button');
    signOutButton.innerText = 'Sign out';
    signOutButton.dataset.section = 'welcomePage';

    telegramBlock.appendChild(telegramDescription);
    telegramBlock.appendChild(telegramLink);

    userBlock.appendChild(nameBlock);
    userBlock.appendChild(lastnameBlock);
    userBlock.appendChild(emailBlock);
    userBlock.appendChild(telegramBlock);
    signoutBlock.appendChild(signOutButton);
    wrapper.appendChild(userBlock);
    wrapper.appendChild(signoutBlock);

    root.appendChild(wrapper);
    console.log('% Main page rendered');

    meRequest().then(({ name, lastname, email, telegramCode, telegram }) => {
      nameBlock.innerText = name;
      lastnameBlock.innerText = lastname;
      emailBlock.innerText = email;
      if (telegram) {
        telegramBlock.style.display = 'none';
      } else {
        telegramDescription.innerText = `Yore telegram code is: ${telegramCode}`;
      }
    });

    signOutButton.addEventListener('click', (event) => {
      event.preventDefault();
      signOutRequest();

      redirect('welcomePage');
    });
  };

  return {
    render,
  };
};

export const SignInPage = (root) => {
  let wrapper;

  const render = () => {
    root.innerHTML = '';

    wrapper = document.createElement('div');
    wrapper.className = 'sign_in_component';

    const description = document.createElement('div');
    description.innerText = 'Beautiful web page';

    const signUpButton = document.createElement('button');
    signUpButton.className = 'signup_button';
    signUpButton.innerText = 'Sign up';

    const signInButton = document.createElement('button');
    signInButton.className = 'signin_button';
    signInButton.innerText = 'Sign in';

    const telegramButton = document.createElement('button');
    telegramButton.className = 'telegram_button';
    telegramButton.innerText = 'Sign in with telegram';
    telegramButton.dataset.section = 'telegramEmail';

    wrapper.appendChild(description);
    wrapper.appendChild(signUpButton);
    wrapper.appendChild(signInButton);
    wrapper.appendChild(telegramButton);

    root.append(wrapper);

    const SignUpForm = SignUpComponent(root);
    const SignInForm = SignInComponent(root);

    signInButton.addEventListener('click', (event) => {
      event.preventDefault();
      SignInForm.render();
    });
    signUpButton.addEventListener('click', (event) => {
      event.preventDefault();
      SignUpForm.render();
    });
  };

  return {
    render,
  };
};

export const TelegramEmailPage = (root) => {
  let wrapper;

  const render = () => {
    root.innerHTML = '';

    wrapper = document.createElement('form');
    wrapper.className = 'telegram_signin';

    const containerBlock = document.createElement('div');
    containerBlock.className = 'container';

    const emailContainer = document.createElement('div');
    emailContainer.className = 'input-container email';
    const buttonContainer = document.createElement('div');
    buttonContainer.className = 'button-wrapper';

    const emailLabel = document.createElement('label');
    emailLabel.for = 'email';
    emailLabel.innerText = 'Your email';
    const emailInput = document.createElement('input');
    emailInput.id = 'email';
    emailInput.type = 'email';
    emailInput.tabindex = 0;

    const sendEmailButton = document.createElement('button');
    sendEmailButton.innerText = 'Send code to telegram';

    emailContainer.appendChild(emailLabel);
    emailContainer.appendChild(emailInput);
    buttonContainer.appendChild(sendEmailButton);

    containerBlock.appendChild(emailContainer);
    containerBlock.appendChild(buttonContainer);

    wrapper.appendChild(containerBlock);

    root.appendChild(wrapper);

    sendEmailButton.addEventListener('click', (event) => {
      event.preventDefault();
      getTelegramCodeRequest({ email: emailInput.value });
      redirect('telegramCode');
    });
  };

  return {
    render,
  };
};

export const TelegramCodePage = (root) => {
  let wrapper;

  const render = () => {
    root.innerHTML = '';

    wrapper = document.createElement('form');
    wrapper.className = 'telegram_signin';

    const containerBlock = document.createElement('div');
    containerBlock.className = 'container';

    const emailContainer = document.createElement('div');
    emailContainer.className = 'input-container email';
    const buttonContainer = document.createElement('div');
    buttonContainer.className = 'button-wrapper';

    const codeLabel = document.createElement('label');
    codeLabel.for = 'code';
    codeLabel.innerText = 'Code from telegram bot';
    const codeInput = document.createElement('input');
    codeInput.id = 'code';
    codeInput.type = 'text';
    codeInput.tabindex = 0;

    const sendCodeButton = document.createElement('button');
    sendCodeButton.innerText = 'Sign in with code';
    emailContainer.appendChild(codeLabel);
    emailContainer.appendChild(codeInput);
    buttonContainer.appendChild(sendCodeButton);

    containerBlock.appendChild(emailContainer);
    containerBlock.appendChild(buttonContainer);

    wrapper.appendChild(containerBlock);

    root.appendChild(wrapper);

    sendCodeButton.addEventListener('click', (event) => {
      event.preventDefault();
      checkTelegramCodeRequest({ code: codeInput.value }).then(() =>
        redirect('mainPage'),
      );

      // console.error('checkTelegramCodeRequest error: ', error);
    });
  };

  return {
    render,
  };
};
