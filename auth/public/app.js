import { application as app, routes } from './utils.js';

document.addEventListener('DOMContentLoaded', application);

const backend = 'http://localhost:9090';

async function application() {
  window.store = {
    auth: false,
  };

  app.addEventListener('click', function (event) {
    const { target } = event;

    if (target.dataset?.section) {
      event.preventDefault();
      app.innerHTML = '';

      if (routes[target.dataset.section].auth && !window.store.auth) {
        routes['welcomePage'].render();
      } else {
        routes[target.dataset.section].render();
      }
    }
  });

  await routes['welcomePage'].render();
}
