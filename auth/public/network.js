export const backend = 'http://localhost:9090';

export const signUpRequest = (sendData) =>
  fetch(`${backend}/singup`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify(sendData),
  })
    .then((resp) => resp.json())
    .then((data) => {
      if (data?.id) {
        window.store.auth = data.id;
        console.log('% Loged in');
      }
    });

export const signInRequest = (sendData) =>
  fetch(`${backend}/signin`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify(sendData),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data?.id) {
        window.store.auth = data.id;
      }
    });

export const signOutRequest = () => {
  fetch(`${backend}/signout`, {
    credentials: 'same-origin',
    method: 'POST',
  }).then(() => {
    window.store.auth = false;
  });
};

export const meRequest = () =>
  fetch(`${backend}/me`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin',
  }).then((res) => res.json());

export const getTelegramCodeRequest = (sendData) =>
  fetch(`${backend}/auth/telegram/email`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify(sendData),
  }).then((res) => res.json());

export const checkTelegramCodeRequest = (sendData) =>
  fetch(`${backend}/auth/telegram/code`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify(sendData),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data?.id) {
        window.store.auth = data.id;
      }
    });
