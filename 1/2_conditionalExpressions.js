'use strict';
console.clear();

// if/else
let animal = {};

// if (animal) {
//   console.log('Animal here!');
// } else {
//   console.log('Animal hidden');
// }

// syntax sugar of logic: ?: &&

// const message = animal ? 'Animal here!' : 'Animal hidden';
// console.log('message is: ', message);

// animal ? console.log('Animal here!') : console.log('Animal hidden');

// animal && console.log('Animal here!');

// switch/case/default

switch (animal) {
  case 'leo':
    console.log('It is leopard!');
    break;
  case 'tiger':
    console.log('It is tiger!');
    break;
  case 'cat':
  case 'dog':
  case 'parrot':
    console.log('It is a pet!');
    break;
  default:
    console.log('Something strange!');
    break;
}
