'use strict';
console.clear();

// cycles common: for while do&while break&continue

const array = [1, { as: 'assa' }, '33', null, 2113n];

// for (let index = 0; index < array.length; index++) {
//   const element = array[index];
//   console.log('Element is: ', element);
// }

// let counter = 5;

// console.log('While cycle:\n');
// while (counter--) {
//   console.log('Counter is: ', counter);
// }

// counter = 5;

// console.log('\nDoWhile cycle:\n');
// do {
//   console.log('Counter is: ', counter);
// } while (counter--);

// counter = 10;
// let iteration = 0;

// console.log('While cycle with continue:\n');
// while (counter--) {
//   console.log('iteration: ', iteration++);
//   if (counter % 2) {
//     console.log('Counter is: ', counter);
//     continue;
//   }
//   if (counter < 3) {
//     break;
//   }
// }

// cycles js: forEach

// array.forEach((element, index, arr) => console.log(element, index, arr, '\n'));
