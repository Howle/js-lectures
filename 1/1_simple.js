'use strict';
// console.clear();
// variables

let a = 23;
const str = 'str1';
var b = 12;

// str = 'new str';

// function func() {
//   {
//     var localVar = '!!! localVar';
//     let localLet = '!!! localLet';
//   }
//   console.log('let a is: ', a);
//   console.log('var b is: ', b);
//   console.log('Function scoped var localVar is: ', localVar);
//   // console.log('Block scoped let localLet is: ', localLet);
// }

// func();

// console.log('Function scoped var localVar in global scope is: ', localVar);

// var b = 'new b';
// console.log('var b re-declared is: ', b);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Hoisting

// var hoisting

// console.log('Tree is: ', tree);
// var tree = 'oak';

//    ^^^

// var tree;
// console.log('Tree is: ', tree);
// tree = 'oak';

// Function hoisting

// catName('Tiger');
// function catName(name) {
//   console.log("My cat's name is " + name);
// }

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// TYPES

let smth;
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// Simple

smth = undefined;
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// smth = true; // false
smth = Boolean(0); // 0000000000000
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// smth = 1;
// smth = Number('321');
// smth = Number([]);
// console.log('Variable is: ', smth, ' type is: ', typeof smth);
// smth = 2 / +0;
// console.log('Variable is: ', smth, ' type is: ', typeof smth);
// smth = -Infinity;
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// smth = '1 is a string';
// console.log('Variable is: ', smth, ' type is: ', typeof smth);
// smth = '2 is a string';

// smth = String(132);

// console.log('Variable is: ', smth, ' type is: ', typeof smth);
// smth = `3 is a string`;
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// smth = null;
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// smth = 23423424n;
// smth = BigInt(5555);
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// smth = Symbol('foo');
// console.log('Variable is: ', smth, ' type is: ', typeof smth);

// console.log(Symbol('foo') == Symbol('foo'));

// Difficult

smth = {
  name: 'NodeJS',
  version: '16.0.3',
  packages: [
    { eslint: '5.6.8' },
    { react: '10.4.3' },
    { mobx: '3.4.3' },
    { 'mui-core': '6.3.31' },
    { 'react-hooks': '11.0.8' },
  ],
  usage: undefined,
  owner: null,
  power: true,
};
// console.log('Variable is: ', smth, '\ntype is: ', typeof smth);

// console.log(smth.packages[0]);
// console.log(smth.usage);

// smth.packages = 'qwant';
// console.log('Variable is: ', smth, '\ntype is: ', typeof smth);

// const copy = { ...smth };
// let tmp
// Object.assign(tmp, smth);
// console.log('Copy is: ', copy, '\ntype is: ', typeof copy);

// copy.ass = 'ASSS!!!!!';
// console.log('Copy is: ', copy, '\ntype is: ', typeof copy);
// console.log('Variable is: ', smth, '\ntype is: ', typeof smth);
// console.log(
//   'Variable is: ',
//   Object.entries(smth),
//   '\ntype is: ',
//   typeof Object.entries(smth),
// );
// console.log(
//   'Variable is: ',
//   Object.keys(smth),
//   '\ntype is: ',
//   typeof Object.keys(smth),
// );

// https://medium.com/youstart-labs/javascript-object-methods-every-developer-should-know-c68c132a658

// smth = JSON.stringify(smth);
// console.log('Variable is: ', smth, '\ntype is: ', typeof smth);

// smth = JSON.parse(smth);
// console.log('Variable is: ', smth, '\ntype is: ', typeof smth);

// smth = [1, '45', 343n, true, null, { object: 'some object' }, undefined];
// console.log('Variable is: ', smth, '\ntype is: ', typeof smth);
//learn.javascript.ru/array-methods

// function out(mess) {
// console.log('Out is: ', mess);
// }

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Comparison

// console.log("1 == '1' is: ", 1 == '1');
// console.log("1 === '1' is: ", 1 === '1');
// console.log('1 === 1 is: ', 1 === 1);
// console.log('1 === 2 is: ', 1 === 2);

// !!! Сompared by reference !!!

// console.log('{} == {} is: ', {} == {});
// console.log('{} === {} is: ', {} === {});

// console.log('[] == [] is: ', [] == []);

// console.log('Symbol comparing: ', Symbol('foo') === Symbol('foo'));

// operations

// tutorialstonight.com/js/js-arithmetic.php
