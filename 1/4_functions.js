'use strict';

// function

// function print(mes) {
//   console.log(mes);
// }

// arrow function

// const print = (mes) => mes;
// const print = (mes) => {
//   return mes
// };

// const print = (mes) => {
//   console.log(mes);
//   return 111;
// };
// const print = console.log;

// const output = print('QWERTY');
// console.log('Output: ', output);

const str = 'str';

function test() {
  const foo = 'foo';
  console.log('str is: ', str);
  const arrTest = () => {
    console.log('str is: ', foo);
  };
  arrTest();
}

console.log('this.str: ', this.str);

test();

// function vs arrow function

// this

// const str = 'str'
// console.log(this);

// function tmp() {
//   console.log(this);
// }

// callback
